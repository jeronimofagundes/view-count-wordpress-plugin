<?php
/*
Plugin Name: View Count
Description: Implements a very basic post and page view count. You need to call 'wp_add_view(get_the_ID())' in single.php to increment views. Use 'wp_get_views(get_the_ID())' to get the number of views of a post.
Version: 1
Requires at least: 3.9.1
Tested up to: 3.9.1
Author: Jeronimo Fagundes da Silva
Author URI: http://jeronimofagund.es
License: MIT
*/
?>
<?php

//register_activation_hook(__FILE__, 'view_count_install');
//register_deactivation_hook(__FILE__, 'view_count_deactivation');


function wp_add_view($postid) {
    global $wpdb;

    $affectedRows = $wpdb->query(
        $wpdb->prepare(
            "UPDATE {$wpdb->postmeta} SET meta_value = meta_value + 1 WHERE post_id = %d AND meta_key = %s",
            $postid,
            'view_count'
        )
    );

    if ($affectedRows === false || $affectedRows === 0) {
        $wpdb->query(
            $wpdb->prepare(
                "INSERT INTO {$wpdb->postmeta} (post_id, meta_key, meta_value) VALUES (%d, %s, %d)",
                $postid,
                'view_count',
                1
            )
        );
    }
}

function wp_get_views($postid) {
    global $wpdb;
    $views = $wpdb->get_var(
        $wpdb->prepare(
            "SELECT COALESCE(SUM(meta_value), 0) AS views FROM {$wpdb->postmeta} WHERE post_id = %d AND meta_key = %s",
            $postid,
            'view_count'
        )
    );
    return $views;
}

?>
